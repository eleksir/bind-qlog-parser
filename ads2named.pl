#!/opt/bin/perl
# vim:set ts=4 sts=4 tw=4 sw=4 ft=perl et noai ci encoding=utf-8 :

# разборщик логов запросов bind'а и генератор stub-зон
# для dnsmasq в (нотации команд) mikrotik и bind'а
#
# не совсем понятно как вычищать устаревшие записи из mikrotik,
# нативных средств у оболочки mikroik нету.


use warnings "all";
use strict;

sub parselog();
sub genzones(@);
sub genconfig(@);
sub genmikrotikfile(@);

my @list = parselog();

genconfig(@list);
genzones(@list);
genmikrotikfile(@list);

exit 0;


sub parselog(){
    open (DOM, "baddomains.txt")||die "no baddomains.txt found";
    my @baddomains;
    foreach(<DOM>){
        my $dom = $_;
        chomp $dom;
        if($dom ne ''){ push @baddomains, $dom; }
    }
    close DOM;
    open (DATA, "/share/Public/logs/named/query.log")||die "No /share/Public/logs/named/query.log found";
    my $str = '';
    my %list;
    while($str = <DATA>){
        $str = substr($str, 50);
        if($str =~ m/\: query\: (.*) IN /){
            $str = lc($1);
            foreach (@baddomains){
                if($str =~ /$_/){ $list{$str} = 1;}
            }
        }
    }

    @baddomains = -1; undef @baddomains;

# добавим хосты слежки от M$
    open (HOSTS2, "hosts2.txt")||die "hosts2.txt not found";
    while(my $h2 = <HOSTS2>){
        chomp $h2;
        if($h2 !~ /^$/){ $list{$h2} = 1; }
    }
    close DATA;

    return keys(%list);
}

sub genmikrotikfile(@){
    open(DATA, ">mikrotik.txt")||die "unable to open mikrotik.txt";
    foreach(@_){ print DATA "add address=127.0.0.1 name=$_\n"; }
    close DATA;
}

sub genconfig(@){
    open(DATA, ">blackhole.conf")||die "unable to open blackhole.conf";
    foreach(@_){
        my $str = $_;
        print DATA <<EOF;;
	zone "$str" IN {
		type master;
		file "blackhole/$str.zone";
		allow-update { none; };
	};
EOF
    }
    close DATA;
}

sub genzones(@){
    foreach(@_){
        my $zone = $_;
        open(DATA, ">blackhole/$zone.zone")||die "unable to open blackhole/$zone.zone";
        print DATA <<DATA;
\$TTL    86400
\$ORIGIN $zone.
\@                       1D IN SOA      $zone. admin.local (
                                        03              ; serial num
                                        9H              ; refresh
                                        15M             ; retry
                                        1W              ; expiry
                                        1D )            ; minimum

                        1D IN NS        array.local.
@			IN A 192.168.88.2
DATA
        close DATA;
    }
}

__END__

