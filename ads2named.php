#!/usr/local/apache/bin/php
<?php

function genmikrotikfile($array){
    $string = '';
    foreach($array as &$element){
        $string .= sprintf("add address=192.168.88.10 name=%s\n", $element);
    }
    file_put_contents("mikrotik.txt", $string);
    unset ($element);
    unset ($string);
}

function genzones($array){
    foreach($array as &$zone){
        $DATA = fopen("blackhole/$zone.zone", "w");
        fprintf($DATA, "\$TTL    86400
\$ORIGIN %s.
@                       1D IN SOA      %s. admin.local (
                                        03              ; serial num
                                        9H              ; refresh
                                        15M             ; retry
                                        1W              ; expiry
                                        1D )            ; minimum

                        1D IN NS        array.local.
@                          IN A         192.168.88.10
", $zone, $zone);
        fclose($DATA);
        unset ($zone);
    }
}

function genconfig($array){
    $string = '';
    foreach($array as &$str){
        $string .= sprintf("zone \"%s\" IN {
        type master;
        file \"blackhole/%s.zone\";
        allow-update { none; };
};
", $str, $str);
    }

    file_put_contents("blackhole.conf", $string);
    unset($str);
    unset($string);
}

function parselog(){
    $DATA = fopen("baddomains.txt", "r");
    $baddomains = array();
    while($string = rtrim(fgets($DATA))){
        if($string != ''){ $baddomains[] = $string; }
    }
    fclose($DATA);

/* теперь у нас есть список шаблонов "плохих" доменов
 * доо, :) теперь мы можем в цикле сравнивать выцепленные из лога хосты с этим списком
 */
    $LOG = fopen("/share/Public/logs/named/query.log", "r");
    $hash = array();
    while($str = fgets($LOG)){
         preg_match("/\: query\: (.*) IN /", $str, $m1); // $m1[1] - это вроде как наш hostname из лога
         foreach($baddomains as &$baddomain){
             if(preg_match("/".$baddomain."/", $m1[1])){ $hash[$m1[1]] = 1; }
         }
         unset($baddomain);
    }
/* теперь у нас есть хэш со списком найденных доменов... */
    fclose($LOG);

/* добавляем туда известные домены майкрософта */
    $HOSTS2 = fopen("hosts2.txt", "r");
    while($host = rtrim(fgets($HOSTS2))){
        $hash[$host] = 1;
    }
    fclose($HOSTS2);
    return array_keys($hash);
}

/* а вот тут хренова бизнЕс-логика */
$arr = array();
$arr = parselog();

genmikrotikfile($arr);
genzones($arr);
genconfig($arr);

?>
