#!/bin/bash

cat blackhole.conf >/opt/etc/named/blackhole.conf
rsync -a --del blackhole /opt/var/named

cd /opt/etc/watchman/init.d
./named restart
